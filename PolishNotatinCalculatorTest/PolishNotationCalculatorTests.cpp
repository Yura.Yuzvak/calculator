#include "pch.h"

#include "PolishNotationCalculator.h"

using namespace MyLib;
TEST(PolishNotationCalculatorTests, SetExpression_CurrectExpression_IfRemovedAllSpaces)
{
	PolishNotationCalculator calculator;

	calculator.SetExpression("12 + 2 * ( ( 3 * 4 ) + ( 10 / 5 ) ) ");

	const auto result = calculator.GetExpression();

	EXPECT_EQ(result, "12+2*((3*4)+(10/5))");
}

TEST(PolishNotationCalculatorTests, Calculate_CurrectExpression_CurrectResult)
{
	PolishNotationCalculator calculator;
	calculator.SetExpression("12 + 2 * ( ( 3 * 4 ) + ( 10 / 5 ) ) ");

	const auto result = calculator.Calculate();

	EXPECT_EQ(result, 40.0);
}

TEST(PolishNotationCalculatorTests, Calculate_CountNumberOfOperators_Throw)
{
	PolishNotationCalculator calculator;

	calculator.SetExpression("2++1");

	EXPECT_THROW(calculator.Calculate(), std::exception);
}

TEST(PolishNotationCalculatorTests, Calculate_DivisionByZero_Throw)
{
	PolishNotationCalculator calculator;
	calculator.SetExpression("2/0");

	EXPECT_THROW(calculator.Calculate(), std::exception);
}
TEST(PolishNotationCalculatorTests, Calculate_InvalidNumberOfParentheses_Throw)
{
	PolishNotationCalculator calculator;

	calculator.SetExpression("2+1)");

	EXPECT_THROW(calculator.Calculate(), std::exception);
}

TEST(PolishNotationCalculatorTests, Calculate_EmptyParentheses_Throw)
{
	PolishNotationCalculator calculator;

	calculator.SetExpression("2+()+1");

	EXPECT_THROW(calculator.Calculate(), std::exception);
}

TEST(PolishNotationCalculatorTests, Calculate_UnknownOperand_Throw)
{
	PolishNotationCalculator calculator;

	calculator.SetExpression("2|1");

	EXPECT_THROW(calculator.Calculate(), std::exception);
}