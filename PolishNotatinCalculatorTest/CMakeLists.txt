find_package(GTest REQUIRED)
include_directories(${GTEST_INCLUDE_DIRS})
include_directories("../PolishNotationCalculator/Src")

add_executable(multiply_test PolishNotationCalculatorTests.cpp)
target_link_libraries(multiply_test
 PRIVATE
  GTest::GTest
  GTest::gtest_main
  PolishNotationCalculator
  ${GTEST_LIBRARIES}
  pthread
  )
add_test(multiply_gtests multiply_test)