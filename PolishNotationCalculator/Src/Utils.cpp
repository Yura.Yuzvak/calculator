#include "pch.h"
#include "Utils.h"

using namespace std;

namespace MyLib {
	string SpaceClean(string str)
	{
		erase_if(str, [](char x) { return x == ' ' || x == '\n'; });
		if (str.length() == 0)
			throw runtime_error("Missing input data");

		return str;
	}

	bool IsNumber(const string& str)
	{
		return isdigit(str[0]);
	}

	bool IsOperator(const string& str)
	{
		switch (str[0])
		{
		case '+':
		case '-':
		case '*':
		case '/':
			return true;
		default:
			return false;
		}
	}

	int OperationPriority(const string& str)
	{
		switch (str[0])
		{
		case '(':
			return 0;
		case '+':
		case '-':
			return 1;
		case '*':
		case '/':
			return 2;
		default:
			throw runtime_error((string("Unknown or unsupported operator ") + str).c_str());
		}
	}

	list<string> DivideIntoElements(const string& str)
	{
		if (str.empty())
			throw runtime_error("There is no expression");

		list<string> elements;
		string number;
		for (const char& ch : str)
		{
			if (isdigit(ch))
				number.push_back(ch);
			else
			{
				if (number.length() != 0)
				{
					elements.push_back(number);
					number = "";
				}
				elements.emplace_back(1, ch);
			}
		}
		if (number.length() != 0)
			elements.push_back(number);

		return elements;
	}
}