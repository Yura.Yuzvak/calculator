#pragma once

#include <stack>
#include <list>

namespace MyLib {
	std::string SpaceClean(std::string str);
	bool IsNumber(const std::string&);
	bool IsOperator(const std::string&);
	int OperationPriority(const std::string&);
	std::list<std::string> DivideIntoElements(const std::string&);

	template <class T>
	T GetAndPopStackElement(std::stack<T>& stack)
	{
		const auto element = stack.top();
		stack.pop();
		return element;
	}
}