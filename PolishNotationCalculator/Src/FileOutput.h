#pragma once

#include <fstream>

#include "IOutput.h"

namespace MyLib::Stream {

	class FileOutput : public IOutput
	{
	public:
		FileOutput(const std::string& fileName);
		~FileOutput() override;

		void Write(const std::string& data) override;

	private:
		std::ofstream m_file;
	};
}