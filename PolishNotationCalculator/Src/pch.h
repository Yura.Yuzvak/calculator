#pragma once

#include <string>
#include <vector>
#include <stack>
#include <iostream>

#if __has_include("format")
    #include <format>
#else
    #define FMT_HEADER_ONLY
    #include <fmt/format.h>
    using fmt::format;
#endif

