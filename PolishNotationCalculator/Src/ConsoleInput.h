#pragma once

#include "IInput.h"

namespace MyLib::Stream
{
	class ConsoleInput : public IInput
	{
	public:
		std::string Read() override;
	};
}