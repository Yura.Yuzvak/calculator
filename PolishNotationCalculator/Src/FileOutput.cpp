#include "pch.h"
#include "FileOutput.h"

using namespace std;

namespace MyLib {

	Stream::FileOutput::FileOutput(const std::string& fileName) :m_file(std::ofstream(fileName))
	{
		if (!m_file)
			throw std::runtime_error("Error open file");
	}

	void Stream::FileOutput::Write(const string& data)
	{
		m_file << data;
	}

	Stream::FileOutput::~FileOutput()
	{
		m_file.close();
	}
}