#include "pch.h"
#include "PolishNotationCalculator.h"

#include "Utils.h"

using namespace std;

namespace MyLib
{
	void PolishNotationCalculator::SetExpression(const string& expression)
	{
		m_expression = SpaceClean(expression);
		m_elementsPolishNotation.clear();
	}

	string PolishNotationCalculator::GetExpression()
	{
		return m_expression;
	}

	string PolishNotationCalculator::GetExpressionInPolishNotation()
	{
		string result;
		if (m_elementsPolishNotation.empty())
		{
			m_elementsOfExpression = DivideIntoElements(m_expression);
			GeneratePolishNotation();
		}
		for (const auto& element : m_elementsPolishNotation)
			result += element + ' ';
		result.pop_back();
		return result;
	}

	double PolishNotationCalculator::Calculate()
	{
		if (m_elementsPolishNotation.empty())
		{
			m_elementsOfExpression = DivideIntoElements(m_expression);
			GeneratePolishNotation();
		}

		double result;
		stack<double> stackResult;

		for (const string& element : m_elementsPolishNotation)
		{
			if (IsNumber(element))
			{
				stackResult.push(stod(element));
				continue;
			}

			if (stackResult.size() < 2)
				throw runtime_error("The number of operations does not correspond to the number of numbers");

			const double leftElement = GetAndPopStackElement(stackResult);
			const double rightElement = GetAndPopStackElement(stackResult);

			switch (element[0])
			{
			case '+':
				result = rightElement + leftElement;
				break;
			case '-':
				result = rightElement - leftElement;
				break;
			case '*':
				result = rightElement * leftElement;
				break;
			case '/':
				if (leftElement == 0.0)
					throw runtime_error("Division by zero");

				result = rightElement / leftElement;
				break;
			default:
				throw runtime_error((string("Unknown or unsupported operand=") + element).c_str());
			}
			stackResult.push(result);
		}
		return stackResult.top();
	}

	void PolishNotationCalculator::GeneratePolishNotation()
	{
		stack<string> stackResult;
		int countNumbers = 0;
		int countOperators = 0;
		for (const auto& element : m_elementsOfExpression)
		{
			if (IsNumber(element))
			{
				m_elementsPolishNotation.push_back(element);
				++countNumbers;
			}
			else if (element == "(")
				stackResult.push(element);
			else if (element == ")")
			{
				MovingOperatorsUntilNotReachParenthesis(stackResult);
			}
			else if (IsOperator(element))
			{
				if (!stackResult.empty() && OperationPriority(stackResult.top()) >= OperationPriority(element))
					m_elementsPolishNotation.push_back(GetAndPopStackElement(stackResult));

				stackResult.push(element);
				++countOperators;
			}
			else
				throw runtime_error((string("Unknown or unsupported operand ") + element).c_str());
		}
		if (countNumbers - 1 != countOperators)
			throw runtime_error("The number of operations does not correspond to the number of numbers");

		MovingOperatorsToResult(stackResult);
	}

	void PolishNotationCalculator::MovingOperatorsToResult(std::stack<std::string>& operatorsStack)
	{
		while (!operatorsStack.empty())
		{
			const auto element = operatorsStack.top();
			if (element == "(")
				throw runtime_error("The expression has either an incorrect punctuation mark or mismatched parentheses");

			if (IsOperator(element))
			{
				m_elementsPolishNotation.push_back(element);
				operatorsStack.pop();
			}
			else
				throw runtime_error((string("Unknown or unsupported operand ") + operatorsStack.top()).c_str());
		}
	}

	void PolishNotationCalculator::MovingOperatorsUntilNotReachParenthesis(std::stack<std::string>& operatorsStack)
	{
		bool ifParsingSuccess = false;
		while (!operatorsStack.empty())
		{
			if (operatorsStack.top() == "(")
			{
				operatorsStack.pop();
				ifParsingSuccess = true;
				break;
			}

			m_elementsPolishNotation.push_back(GetAndPopStackElement(operatorsStack));
		}
		if (!ifParsingSuccess)
			throw runtime_error("The expression has either an incorrect punctuation mark or mismatched parentheses");
	}
}
