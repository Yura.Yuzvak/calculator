#pragma once
#include <string>

namespace MyLib::Stream
{
	class IInput
	{
	public:
		virtual ~IInput() = default;

		virtual std::string Read() = 0;
	};
}