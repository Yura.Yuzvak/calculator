#pragma once

#include <string>
#include <memory>

#include "PolishNotationCalculator.h"
#include "IInput.h"
#include "IOutput.h"

namespace MyLib
{
	class Engine
	{
	public:
		Engine(std::unique_ptr<Stream::IInput> input, std::unique_ptr<Stream::IOutput> output);

		void Read();
		void Calculate();
		void Print();

	private:
		std::unique_ptr<Stream::IInput> m_input;
		std::unique_ptr<Stream::IOutput> m_output;
		PolishNotationCalculator m_calculator;
		std::string m_out;
		std::string m_resultCalculate;
	};
}
