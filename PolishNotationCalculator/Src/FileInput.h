#pragma once

#include <fstream>

#include "IInput.h"

namespace MyLib::Stream
{
	class FileInput : public IInput
	{
	public:
		FileInput(const std::string& fileName);
		~FileInput() override;

		std::string Read() override;
		
	private:
		std::ifstream m_file;
	};
}
