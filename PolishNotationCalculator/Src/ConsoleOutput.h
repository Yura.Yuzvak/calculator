#pragma once

#include "IOutput.h"

namespace MyLib::Stream {

	class ConsoleOutput : public IOutput
	{
		void Write(const std::string& data) override;
	};
}