#include "pch.h"
#include "Engine.h"

using namespace std;

namespace MyLib {

	Engine::Engine(std::unique_ptr<Stream::IInput> input, std::unique_ptr<Stream::IOutput> output) :
		m_input(std::move(input)),
		m_output(std::move(output))
	{
	}

	void Engine::Read()
	{
		const auto inputData = m_input->Read();
		m_calculator.SetExpression(inputData);
	}

	void Engine::Calculate()
	{
		m_resultCalculate = to_string(m_calculator.Calculate());
	}

	void Engine::Print()
	{
		const string result = format("{}\n{}\n{}",
			m_calculator.GetExpression(),
			m_resultCalculate,
			m_calculator.GetExpressionInPolishNotation()
		);
		m_output->Write(result);
	}
}
