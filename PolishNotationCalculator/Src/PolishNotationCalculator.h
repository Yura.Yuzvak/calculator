#pragma once

#include <list>
#include <string>
#include <stack>

namespace MyLib
{
	class PolishNotationCalculator
	{
	public:
		void SetExpression(const std::string& expression) ;
		std::string GetExpression() ;
		std::string GetExpressionInPolishNotation() ;
		double Calculate();

	private:
		void GeneratePolishNotation();
		void MovingOperatorsToResult(std::stack<std::string>& operatorsStack);
		void MovingOperatorsUntilNotReachParenthesis(std::stack<std::string>& operatorsStack);

		std::string m_expression;
		std::list<std::string> m_elementsPolishNotation;
		std::list<std::string> m_elementsOfExpression;
	};
}
