#include "pch.h"
#include "ConsoleInput.h"

using namespace std;

namespace MyLib
{
	string Stream::ConsoleInput::Read()
	{
		string s;
		getline(cin, s);
		return s;
	}
}