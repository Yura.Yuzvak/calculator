#pragma once

namespace MyLib::Stream {
	class IOutput
	{
	public:
		virtual ~IOutput() = default;

		virtual void Write(const std::string& data) = 0;
	};
}