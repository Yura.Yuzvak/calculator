#include "pch.h"
#include "FileInput.h"

using namespace std;

namespace MyLib::Stream {

	FileInput::FileInput(const string& fileName) : m_file(ifstream(fileName))
	{
		if (!m_file)
			throw std::runtime_error("Error open file");
	}

	string FileInput::Read()
	{
		string line;
		string result;
		while (getline(m_file, line))
		{
			result += line;
		}
		return result;
	}

	FileInput::~FileInput()
	{
		m_file.close();
	}
}
