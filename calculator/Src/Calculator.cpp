#include <iostream>
#include <fstream>
#include <string.h> 

#include "IInput.h"
#include "IOutput.h"
#include "ConsoleInput.h"
#include "FileInput.h"
#include "ConsoleOutput.h"
#include "FileOutput.h"
#include "Engine.h"

using namespace std;

unique_ptr<MyLib::Stream::IInput> MakeInput(int argc, char** argv, int& outputInfoIndex)
{
	constexpr int inputInfoIndex = 1;
	if (!strcmp(argv[inputInfoIndex], "c"))
		return make_unique<MyLib::Stream::ConsoleInput>();

	if (!strcmp(argv[inputInfoIndex], "f"))
	{
		++outputInfoIndex;
		if (argc < outputInfoIndex + 1)
			throw runtime_error("Command arguments are not valid");

		return  make_unique<MyLib::Stream::FileInput>(argv[2]);
	}
	throw runtime_error("Command arguments are not valid");
}

unique_ptr<MyLib::Stream::IOutput> MakeOutput(int argc, char** argv, int consoleOutputIndex)
{
	if (!strcmp(argv[consoleOutputIndex], "c"))
		return  make_unique<MyLib::Stream::ConsoleOutput>();

	if (!strcmp(argv[consoleOutputIndex], "f"))
	{
		if (argc < consoleOutputIndex + 2)
			throw runtime_error("Command arguments are not valid");

		return  make_unique<MyLib::Stream::FileOutput>(argv[consoleOutputIndex + 1]);
	}
	throw runtime_error("Command arguments are not valid");
}

MyLib::Engine Init(int argc, char** argv)
{
	if (argc < 2 + 1 || argc > 4 + 1)
		throw runtime_error("Command arguments are not valid");
		
	int outputInfoIndex = 2;
	auto input = MakeInput(argc, argv, outputInfoIndex);
	auto output = MakeOutput(argc, argv, outputInfoIndex);

	return {std::move(input), std::move(output)};
}

int Start(int argc, char** argv)
{
	try
	{
		MyLib::Engine engine = Init(argc, argv);
		engine.Read();
		engine.Calculate();
		engine.Print();
	}
	catch (const exception& e)
	{
		cout << e.what() << endl;
		return EXIT_FAILURE;
	}
	return EXIT_SUCCESS;
}

int main(int argc, char** argv)//  c/f "name file"  c/f "name file"
{
	return Start(argc, argv);
}
